#! /bin/bash

#Tools needed: 
#resty, downloaded below.
#curl -L https://raw.githubusercontent.com/micha/resty/master/resty > ~/resty


. ~/resty
#test settings for Resty.

# SERVER=localhost:7990

yell() { echo "$0: $*" >&2; }
die() { yell "$*"; exit 1; }

#test data

#export REPO=test
#export PROJECT=test

# export REPO=test1
# export PROJECT="~mbarr"
# export BARTUSER=mbarr

# SERVER=staging.api.git.source.$$.com
# export REPO=$1
# export PROJECT=SOURCES
# export BARTUSER=release-staging.$$.com

SERVER=api.git.source.$$.com
export REPO=$1
export PROJECT=SOURCES
export BARTUSER=release.$$.com


#Instantiate resty
resty "https://$SERVER" -Embarr -H "Content-Type: application/json"


echo $SERVER
echo $REPO
echo $BARTUSER
echo $PROJECT

GET /rest/api/1.0/projects/$PROJECT/repos/$REPO/ >/dev/null 2>&1  || die "$PROJECT/$REPO does not exist on $SERVER"

# --- Below sets all the settings.


PUT "/rest/api/1.0/projects/$PROJECT/repos/$REPO/permissions/users?name=$BARTUSER&permission=REPO_WRITE" -Q


#set branching model up
PUT  /rest/branch-utils/1.0/projects/$PROJECT/repos/$REPO/branchmodel/configuration '{
  "development": {
      "refId": null,
      "useDefault": true
  },
    "types": [
        {
            "displayName": "Bugfix",
            "enabled": true,
            "id": "BUGFIX",
            "prefix": "bugfix/"
        },
        {
            "displayName": "Feature",
            "enabled": true,
            "id": "FEATURE",
            "prefix": "feature/"
        },
        {
            "displayName": "Hotfix",
            "enabled": true,
            "id": "HOTFIX",
            "prefix": "hotfix/"
        },
        {
            "displayName": "Release",
            "enabled": true,
            "id": "RELEASE",
            "prefix": "vc/"
        }
    ]
}'
#enable auto-merge
PUT /rest/branch-utils/1.0/projects/$PROJECT/repos/$REPO/automerge/enabled

#set PR settings

POST /rest/api/1.0/projects/$PROJECT/repos/$REPO/settings/pull-requests '{
    "mergeConfig": {
        "defaultStrategy": {
            "id": "no-ff"
        },
        "strategies": [
            {
                "id": "no-ff"
            }
        ]
    },
    "requiredApprovers": 1,
    "requiredAllApprovers": false,
    "requiredAllTasksComplete": true,
    "requiredSuccessfulBuilds": 0
}'

#prevent rewrite history 
POST /rest/branch-permissions/2.0/projects/$PROJECT/repos/$REPO/restrictions '{
    "type": "fast-forward-only",
    "matcher": {
        "id": "refs/heads/master",
        "displayId": "master",
        "type": {
            "id": "BRANCH",
            "name": "Branch"
        },
        "active": true
    },
    "users": [],
    "groups": []
}'

#prevent master changes w/o PR

POST /rest/branch-permissions/2.0/projects/$PROJECT/repos/$REPO/restrictions '{
    "type": "pull-request-only",
    "matcher": {
        "id": "refs/heads/master",
        "displayId": "master",
        "type": {
            "id": "BRANCH",
            "name": "Branch"
        },
        "active": true
    },
    "users": [],
    "groups": []
}'
#prevent master deletion
POST /rest/branch-permissions/2.0/projects/$PROJECT/repos/$REPO/restrictions '{
    "type": "no-deletes",
    "matcher": {
        "id": "refs/heads/master",
        "displayId": "master",
        "type": {
            "id": "BRANCH",
            "name": "Branch"
        },
        "active": true
    },
    "users": [],
    "groups": []
}'


# vc branch
#prevent rewrite history 
POST /rest/branch-permissions/2.0/projects/$PROJECT/repos/$REPO/restrictions '{
    "type": "fast-forward-only",
    "matcher": {
        "id": "vc/**",
        "type": {
            "id" : "PATTERN",
            "name" : "Pattern"
        },
        "active": true
    },
    "users": [],
    "groups": []
}'

#prevent vc changes w/o PR

POST /rest/branch-permissions/2.0/projects/$PROJECT/repos/$REPO/restrictions '{
    "type": "pull-request-only",
    "matcher": {
        "id": "vc/**",
        "displayId": "vc",
        "type": {
            "id" : "PATTERN",
            "name" : "Pattern"
        },
        "active": true
    },
    "users": [],
    "groups": []
}'

#prevent vc deletion , except bart.
POST /rest/branch-permissions/2.0/projects/$PROJECT/repos/$REPO/restrictions '{
    "type": "no-deletes",
    "matcher": {
        "id": "vc/**",
        "type": {
            "id" : "PATTERN",
            "name" : "Pattern"
        },
        "active": true
    },
    "users": ["'$BARTUSER'"],
    "groups": []
}'

PUT /rest/api/1.0/projects/$PROJECT/repos/$REPO/settings/hooks/com.$$.bitbucket.branchManager.BranchManager:reject-branch-creation-hook/enabled '{
  "blockedPattern": "vc/**",
  "branchHookUsers": "'$BARTUSER'",
  "jsonGroupAndUsers": "{\"users\":[{\"name\":\"'$BARTUSER'\"}],\"groups\":[]}"
}'


#check branch creation settings.
#GET /rest/api/1.0/projects/$PROJECT/repos/$REPO/settings/hooks/com.$$.bitbucket.branchManager.BranchManager:reject-branch-creation-hook/settings




#prevent tag changes
POST /rest/branch-permissions/2.0/projects/$PROJECT/repos/$REPO/restrictions '{
    "type": "read-only",
    "matcher": {
        "id": "refs/tags/version/**",
        "displayId" : "refs/tags/version/**",
        "type": {
            "id" : "PATTERN",
            "name" : "Pattern"
        },
        "active": true
    },
    "users": ["'$BARTUSER'"],
    "groups": []
}'

#prevent deletion
POST /rest/branch-permissions/2.0/projects/$PROJECT/repos/$REPO/restrictions '{
    "type": "no-deletes",
    "matcher": {
        "id": "refs/tags/version/**",
        "type": {
            "id" : "PATTERN",
            "name" : "Pattern"
        },
        "active": true
    },
    "users": [],
    "groups": []
}'

# #set mandatory reviewers. only works on non-personal projects.
POST /rest/workzoneresource/latest/branch/reviewers/$PROJECT/$REPO  '{
    "refPattern": "vc/**",
    "users": [
    ],
    "groups": [
    ],
    "mandatoryUsers": [
      {
        "name": "'$BARTUSER'"
      }
    ]
}'

# Set workflow properties

POST /rest/workzoneresource/latest/workflow/$PROJECT/$REPO '{"projectKey":"'$PROJECT'","repoSlug":"'$REPO'","pushAfterPullReq":true,"unapprovePullReq":true}'


#JIRA/CR required
#GET /rest/api/1.0/projects/$PROJECT/repos/$REPO/settings/hooks/com.$$.stash.plugin.stash-bugzilla-integration:stash-bugzilla-hook/settings
PUT /rest/api/1.0/projects/$PROJECT/repos/$REPO/settings/hooks/com.$$.stash.plugin.stash-bugzilla-integration:stash-bugzilla-hook/enabled '{"prMergeCheck":"either"}'

#-----
#GET /rest/workzoneresource/latest/workflow/$PROJECT/$REPO

#GET /rest/workzoneresource/latest/branch/workflow/DEVTOOLS/tagtest

# GET /rest/workzoneresource/latest/branch/properties/$PROJECT/$REPO
# GET /rest/workzoneresource/latest/branch/reviewers/$PROJECT/$REPO
# GET /rest/workzoneresource/latest/branch/reviewers/DEVTOOLS/tagtest
# GET /rest/workzoneresource/latest/branch/properties/DEVTOOLS/tagtest
