#! /bin/bash

#Tools needed: 
#resty, downloaded below.
#curl -L https://raw.githubusercontent.com/micha/resty/master/resty > ~/resty

#jq  :  https://stedolan.github.io/jq/download/

. ~/resty
#test settings for Resty.

# SERVER=localhost:7990


#test data

#export REPO=test
#export PROJECT=test

# export REPO=test1
# export PROJECT="~mbarr"
# export BARTUSER=mbarr

# SERVER=staging.api.git.source.$$.com
# export REPO=$1
# export PROJECT=SOURCES
# export BARTUSER=release-staging.$$.com

SERVER=api.git.source.$$.com
export REPO=$1
export PROJECT=SOURCES
export BARTUSER=release.$$.com


#Instantiate resty
resty "https://$SERVER" -Embarr -H "Content-Type: application/json"
mkdir -p repos
pushd repos
#Create the repo, and capture the clone URL.  Needs jq installed in your path!
export CLONEURL=`POST /rest/api/1.0/projects/$PROJECT/repos '{"name": "'$REPO'"}'|jq -r  '.links.clone[]| select(.name=="ssh")|.href'`

#exportCLONEURL=ssh://git@git.source.$$.com:7999/sources/canonical-test.git
sleep 3
echo $CLONEURL
git clone $CLONEURL
cd $REPO
echo "Initial Commit for $PROJECT/$REPO." > README
git add README
git commit -m "Initial commit for $PROJECT/$REPO."
git rm README
git commit -m "Clean up fake readme file."
git push origin master
cd ..
popd


# --- Below sets write permission for BaRT.

#echo PUT "/rest/api/1.0/projects/$PROJECT/repos/$REPO/permissions/users?name=$BARTUSER&permission=REPO_WRITE" -Q
PUT "/rest/api/1.0/projects/$PROJECT/repos/$REPO/permissions/users?name=$BARTUSER&permission=REPO_WRITE" -Q
